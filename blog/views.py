from django.core.paginator import PageNotAnInteger, EmptyPage, Paginator
from django.shortcuts import render
from django.views import generic, View
from django.views.generic import TemplateView

from .filters import BlogFilter
from .models import Article, Category


####### ARTICLES #######
def article_list(request):
    article_list = Article.objects.all()
    category_list = Category.objects.all()


    # FILTERS
    myFilter = BlogFilter(request.GET, queryset=article_list)
    article_list = myFilter.qs

    paginator = Paginator(article_list, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'articles': page_obj,
        'categories': category_list,
        'myFilter': myFilter,
    }
    return render(request, 'blog/article_list.html', context)


class ArticleDetail(generic.DetailView):
    model = Article
    template_name = 'blog/article_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ArticleDetail, self).get_context_data(**kwargs)
        context['article'] = Article.objects.all()
        context['category'] = Category.objects.all()
        return context


####### CATEGORIES #######
def category_list(request):
    category_list = Category.objects.all()

    # PAGINATION
    paginator = Paginator(category_list, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    # FILTERS
    myFilter = BlogFilter(request.GET, queryset=category_list)
    category_list = myFilter.qs

    context = {
        # 'articles': page_obj,
        'categories': page_obj,
        'myFilter': myFilter,
    }
    return render(request, 'blog/category_list.html', context)


def category_list(request):
    category_list = Category.objects.all()

    paginator = Paginator(category_list, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'paginator': page_obj,
        'categories': category_list,
    }
    return render(request, 'blog/category_list.html', context)


class CategoryDetail(generic.DetailView):
    model = Article
    template_name = 'blog/category_detail.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryDetail, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        return context
