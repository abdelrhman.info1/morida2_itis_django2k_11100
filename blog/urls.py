from django.urls import path

from . import views
from .views import ArticleDetail, CategoryDetail

app_name = 'blog'

urlpatterns = [
    # Articles
    path('', views.article_list, name='article_list'),
    path("<slug:slug>", ArticleDetail.as_view(), name="article-detail"),

    # Categories
    path('list/', views.category_list, name='category_list'),
    path("<slug:slug>", CategoryDetail.as_view(), name="category-detail"),


]
