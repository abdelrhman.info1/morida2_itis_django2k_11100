import django_filters

from blog.models import Article


class BlogFilter(django_filters.FilterSet):
    # Search for the element that contains a spacfic keyword.
    title = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Article
        fields = ['title', 'category']
