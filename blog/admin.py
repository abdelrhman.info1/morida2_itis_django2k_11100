from django.contrib import admin

# Register your models here.
from .models import Article, Category


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'status', 'created_on')
    list_filter = ("status",)
    search_fields = ['title', 'content']
    prepopulated_fields = {'slug': ('title',)}


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'created_on']


admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)
