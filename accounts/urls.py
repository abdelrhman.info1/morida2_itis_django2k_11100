from django.urls import path, include

from . import views

app_name = 'accounts'

urlpatterns = [
    path('signup', views.sign_up, name='sign_up'),
    path('profile', views.profile, name='profile'),
    path('profile/edit', views.profile_edit, name='profile_edit'),
]


