# Generated by Django 4.1.3 on 2022-11-13 14:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='about',
            field=models.TextField(default='', max_length=500),
            preserve_default=False,
        ),
    ]
