from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.template.context_processors import request
from django.urls import reverse

from .forms import SignupForm, UserForm, ProfileForm
from .models import Profile


# Create your views here.

def sign_up(request):
    # If method is post, save data and send request
    form = SignupForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password1"]
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('/accounts/profile')

    else:  # just display form
        form = SignupForm()
    return render(request, 'registration/signup.html', {'form': form})


def profile(request):
    profile = Profile.objects.get(user=request.user)
    return render(request, 'accounts/profile.html', {'profile': profile})


def profile_edit(request):
    profile = Profile.objects.get(user=request.user)

    if request.method == 'POST':
        # 1 Passed Data of the two forms.
        userform = UserForm(request.POST, instance=request.user)
        # 1.1 Add request.FILES to read images from form.
        profileform = ProfileForm(request.POST, request.FILES, instance=profile)
        # 2 Check if the two forms are valid.
        if userform.is_valid() and profileform.is_valid():
            # 3 Save user form.
            userform.save()
            # 4 Stop profile form from being saved.
            myprofile = profileform.save(commit=False)
            # 5 Passed user data to my profile form
            myprofile.user = request.user
            # 6 Saved my profile form.
            myprofile.save()
            return redirect(reverse('accounts:profile'))
    else:
        userform = UserForm(instance=request.user)
        profileform = ProfileForm(instance=profile)

    context = {
        'userform': userform,
        'profileform': profileform,
    }

    return render(request, 'accounts/profile_edit.html', context)
