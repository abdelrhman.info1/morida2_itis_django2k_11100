from django.urls import  path

from . import views

app_name = 'jobs'

urlpatterns = [
    path('', views.job_list, name='job_list'),
    path('create-job', views.create_job, name='create_job'),
    path("<slug:slug>", views.job_detail, name="job_detail"),

    # Categories
    path('categories/', views.categories_list, name='categories_list'),
    path("categories/<slug:slug>/", views.category_list, name="category-list"),
]


