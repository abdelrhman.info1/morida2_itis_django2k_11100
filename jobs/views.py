from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic
from django.views.generic import DetailView

from .filters import JobFilter
from .models import Job, Category
from .forms import Apply, ApplyForm, JobForm


# Create your views here.
def job_list(request):
    job_list = Job.objects.all()
    category_list = Category.objects.all()

    ## FILTERS
    myFilter = JobFilter(request.GET, queryset=job_list)
    job_list = myFilter.qs

    paginator = Paginator(job_list, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'jobs': page_obj,
        'categories': category_list,
        'myFilter': myFilter,
    }
    return render(request, 'job/job_list.html', context)





def job_detail(request, slug):
    job_detail = Job.objects.get(slug=slug)

    if request.method == 'POST':
        form = ApplyForm(request.POST, request.FILES)
        if form.is_valid():
            myform = form.save(commit=False)
            myform.job = job_detail
            myform.save()
            print('DONE')
    else:
        form = ApplyForm()

    context = {
        'jobs': job_detail,
        'forms': form,
    }
    return render(request, 'job/job_detail.html', context)


def create_job(request):
    if request.method == 'POST':
        form = JobForm(request.POST, request.FILES)
        if form.is_valid():
            myform = form.save(commit=False)  # Don't save in DB Yet
            myform.owner = request.user
            myform.save()
            return redirect(reverse('jobs:job_list'))
    else:
        form = JobForm()

    context = {
        'form': form,
    }
    return render(request, 'job/create_job.html', context)


def categories_list(request):
    category_list = Category.objects.all()

    paginator = Paginator(category_list, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'paginator': page_obj,
        'categories': category_list,
    }
    return render(request, 'job/categories_list.html', context)


def category_list(request, slug):
    jobs_list = Category.objects.filter(slug=slug)

    paginator = Paginator(jobs_list, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'paginator': page_obj,
        'jobs_list': jobs_list,
    }
    return render(request, 'job/category_list.html', context)
