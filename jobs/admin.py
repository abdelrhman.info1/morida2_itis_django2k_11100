from django.contrib import admin

# Register your models here.
from .models import Job, Category, Apply


class JobAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)


class CategoryAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)


class ApplyAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)


admin.site.register(Job, JobAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Apply, ApplyAdmin)
