from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from pyexpat import model


# To customize how user uploads his image into your media file
def upload_location(instance, filename):
    filebase, extension = filename.split('.')
    return 'images/%s.%s' % (instance.title, extension)


class Job(models.Model):
    JOB_TYPE = (
        ('Full Time', 'Full Time'),
        ('Part Time', 'Part Time'),
    )

    owner = models.ForeignKey(User, related_name='job_owner', on_delete=models.CASCADE)
    title = models.CharField(max_length=101)
    slug = models.SlugField(unique=True, max_length=200, blank=True)
    # location
    job_type = models.CharField(max_length=15, choices=JOB_TYPE)
    description = models.TextField(max_length=1000, null=True, blank=True)
    published_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    vacancy = models.IntegerField(default=0)
    salary = models.IntegerField(default=0)
    experience = models.IntegerField(default=0)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, null=True, default='Category')
    image = models.ImageField(upload_to=upload_location, null=True, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('jobs:job_detail', kwargs={"slug": self.slug})

    # To Slugify a URL
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super().save(*args, **kwargs)


class Category(models.Model):
    name = models.CharField(max_length=25)
    slug = models.SlugField(max_length=200, blank=True)
    description = models.TextField(max_length=1000, null=True, blank=True)
    created_on = models.DateTimeField(auto_now=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    class Meta:
        ordering = ['-created_on']
        verbose_name = ('Category')
        verbose_name_plural = ('Categories')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('jobs:job_list', kwargs={"slug": self.slug})

    # To Slugify a URL
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class Apply(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=100)
    website = models.URLField(max_length=200)
    cv = models.FileField(upload_to='apply/')
    cover_letter = models.CharField(max_length=500)
    job = models.ForeignKey(Job, related_name='apply_job', on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now=True, null=True, blank=True)
    updated_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    class Meta:
        ordering = ['-created_on']
        verbose_name = ('Apply')
        verbose_name_plural = ('Applications')

    def __str__(self):
        return self.name
