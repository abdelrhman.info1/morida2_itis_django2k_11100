import django_filters

from jobs.models import Job


class JobFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Job
        fields = ['title', 'job_type', 'category']
