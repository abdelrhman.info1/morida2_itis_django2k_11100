from django.core.paginator import Paginator
from django.shortcuts import render
from django.views import generic

from jobs.filters import JobFilter
from jobs.models import Job, Category


def home_page(request):
    job_list = Job.objects.all()
    category_list = Category.objects.all()

    # FILTERS
    my_filter = JobFilter(request.GET, queryset=job_list)
    job_list = my_filter.qs

    context = {
        'jobs': job_list,
        'myFilter': my_filter,
        'categories': category_list,
    }
    return render(request, 'home/home.html', context)

