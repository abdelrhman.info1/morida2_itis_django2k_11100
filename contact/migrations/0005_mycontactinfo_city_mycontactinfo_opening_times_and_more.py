# Generated by Django 4.1.3 on 2022-11-12 17:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0004_rename_contactinfo_mycontactinfo'),
    ]

    operations = [
        migrations.AddField(
            model_name='mycontactinfo',
            name='city',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mycontactinfo',
            name='opening_times',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mycontactinfo',
            name='street',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]
