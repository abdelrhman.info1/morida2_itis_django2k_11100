from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


# Create your models here.
class MyContactInfo(models.Model):
    # Fields
    email = models.EmailField(max_length=50)
    place = models.CharField(max_length=50)
    phone = PhoneNumberField(null=False, blank=False, unique=True)
    # city = models.CharField(max_length=100)
    # street = models.CharField(max_length=100)
    # opening_times = models.CharField(max_length=100)


    class Meta:
        verbose_name = 'Our Contact Info'
        verbose_name_plural = 'Our Contact Info'

    def __str__(self):
        return self.email
