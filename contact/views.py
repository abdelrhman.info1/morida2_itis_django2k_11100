from django.shortcuts import render, redirect
from .forms import ContactForm
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse

from .models import MyContactInfo


# Create your views here.

def contactMessage (request):
    info = MyContactInfo.objects.all()
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = "Website Inquiry"
            body = {
                'first_name': form.cleaned_data['first_name'],
                'last_name': form.cleaned_data['last_name'],
                'email': form.cleaned_data['email_address'],
                'message': form.cleaned_data['message'],
            }
            message = "\n".join(body.values())

            try:
                send_mail(subject, message, 'abdelrhman.info1@gmail.com', ['abdelrhman.info1@gmail.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect("home:home")

    form = ContactForm()
    return render(request, 'contact/contact.html', {'form': form , 'info': info})

