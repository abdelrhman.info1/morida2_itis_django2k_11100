asgiref==3.5.2
beautifulsoup4==4.11.1
colorama==0.4.6
Django==4.1.3
django-bootstrap-v5==1.0.11
django-bootstrap4==22.2
django-countries==7.4.2
django-filter==22.1
django-js-asset==2.0.0
django-model-utils==4.2.0
django-mptt==0.14.0
django-phone-field==1.8.1
form==0.0.1
Pillow==9.3.0
psycopg2-binary==2.9.5
six==1.16.0
soupsieve==2.3.2.post1
sqlparse==0.4.3
swapper==1.3.0
tqdm==4.64.1
typing_extensions==4.4.0
tzdata==2022.6
